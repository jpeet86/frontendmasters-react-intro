const path = require('path')

module.exports = {
  // Route directory
  context: __dirname,
  // Front door into application
  entry: './js/ClientApp.js',
  // Same as source-maps
  devtool: 'eval',
  output: {
    path: path.join(__dirname, '/public'),
    filename: 'bundle.js'
  },
  resolve: {
    /* 'import Blah from Blah' will first check for a file Blah if none,
       then Blah.js then Blah.json */
    extensions: ['.js', '.json']
  },
  stats: {
    colours: true,
    // what caused the issue
    reasons: true,
    chunks: true
  },
  // All the transform webpack to apply
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.js$/,
        loader: 'eslint-loader',
        exclude: /node_modules/
      },
      {
        // Dont run node_modules through babel
        // Could also just include: path.resolve(__dirname, 'js')
        exclude: /node_modules/,
        // Regex - if it ends in js run it through
        test: /\.js$/,
        // If it passes this test run through babel-loader
        loader: 'babel-loader'
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            // So webpack can read CSS
            loader: 'css-loader',
            options: { url: false }
          }
        ]
      }
    ]
  }
}
